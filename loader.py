import itertools
from csv import reader
import cv2 as cv
import os
import random
import torch as torch

from ImagesDataset import ImagesDataset


class ImagesDatasetWithIndeces(ImagesDataset):
    def __getitem__(self, index):
        data, name, cr = ImagesDataset.__getitem__(self, index)
        return data, name, cr, index


class Loader:
    def __init__(self, device):
        self.training = ImagesDatasetWithIndeces()
        self.validation = ImagesDatasetWithIndeces()
        self.test = ImagesDatasetWithIndeces()
        self.all = ImagesDatasetWithIndeces()

        self.bags = {}
        self.bags_birth_date = {}
        self.features_path = 'features/morph_bags_adjusted.csv'
        self.CPU_SAMPLE_SIZE = 300
        self.bags_creation_years, self.real_birth_years, self.birth_ranges, self.feasible_years, self.birth_priors \
            = self.load_for_cpu() if device.type == 'cpu' else self.load()

    @staticmethod
    def generate_birth_date():
        start_year = 1900
        end_year = 2020
        random_range = random.randrange(end_year - start_year)
        return start_year + random_range

    def get(self):
        return self.bags_creation_years, self.real_birth_years, self.birth_ranges, self.feasible_years, self.birth_priors

    def load(self):
        bags = {}
        bags_creation_years = {}
        real_birth_years = {}
        birth_ranges = {}

        with open(self.features_path, 'r') as read_obj:

            data = reader(read_obj)

            for features in itertools.islice(data, 1, None):
                img = cv.imread(os.path.join('data/', features[0]))

                if img is None:
                    continue
                gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
                name = features[9]

                if name not in bags:
                    bags[name] = torch.Tensor()
                    bags_creation_years[name] = torch.Tensor()
                    real_birth_years[name] = self.generate_birth_date()

                bd = real_birth_years[name]
                age = int(features[10])
                folder = int(features[13])

                self.all.push((gray, name, bd + age))

                if folder in range(4, 11):
                    self.training.push([gray, name, bd + age])

                elif folder in range(3, 4):
                    self.validation.push([gray, name, bd + age])

                else:
                    self.test.push([gray, name, bd + age])

                bags_creation_years[name] = torch.cat((bags_creation_years[name], torch.Tensor([bd + age])))
                bags[name] = torch.cat((bags[name], torch.Tensor(gray)))

                # init info
            for bag in list(bags):
                if len(bags[bag]) == 0:
                    del bags[bag]
                    del bags_creation_years[bag]
                    del real_birth_years[bag]

            for bag in bags:
                min_cr = torch.min(bags_creation_years[bag]).long()
                max_cr = torch.max(bags_creation_years[bag]).long()
                period = 99 - (max_cr - min_cr)
                birth_ranges[bag] = torch.Tensor([min_cr - period, min_cr])
            min_over_all = torch.min(torch.t(torch.cat(list(birth_ranges.values())).view(-1, 2))[0])
            max_over_all = torch.max(torch.t(torch.cat(list(birth_ranges.values())).view(-1, 2))[1])
            feasible_years = torch.arange(min_over_all.item(), max_over_all.item() + 1)
            birth_priors = torch.Tensor([100 / feasible_years.size()[0] / 100]).repeat(feasible_years.size()[0])

            return bags_creation_years, real_birth_years, birth_ranges, feasible_years, birth_priors

    def load_for_cpu(self):

        train_count = 0
        valid_count = 0
        test_count = 0
        all_count = 0

        bags = {}
        bags_creation_years = {}
        real_birth_years = {}
        birth_ranges = {}

        with open(self.features_path, 'r') as read_obj:
            data = reader(read_obj)

            for features in itertools.islice(data, 1, None):
                img = cv.imread(os.path.join('data/', features[0]))

                if img is None:
                    continue
                gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
                name = features[9]

                if name not in bags:
                    bags[name] = torch.Tensor()
                    bags_creation_years[name] = torch.Tensor()
                    real_birth_years[name] = self.generate_birth_date()

                bd = real_birth_years[name]
                age = int(features[10])
                folder = int(features[13])
                if folder in range(4, 11):
                    if train_count == self.CPU_SAMPLE_SIZE:
                        continue
                    self.training.push([gray, features[9], bd + age])
                    train_count = train_count + 1
                    self.all.push((gray, features[9], bd + age))
                    all_count = all_count + 1

                elif folder in range(3, 4):
                    if valid_count == self.CPU_SAMPLE_SIZE:
                        continue
                    self.validation.push([gray, features[9], bd + age])
                    valid_count = valid_count + 1
                    self.all.push((gray, features[9], bd + age))
                    all_count = all_count + 1

                else:
                    if test_count == self.CPU_SAMPLE_SIZE / 2:
                        continue
                    self.test.push([gray, features[9], bd + age])
                    test_count = test_count + 1
                    self.all.push((gray, features[9], bd + age))
                    all_count = all_count + 1

                # bd = real_birth_years[name]
                bags_creation_years[name] = torch.cat((bags_creation_years[name], torch.Tensor([bd + age])))
                bags[name] = torch.cat((bags[name], torch.Tensor(gray)))

                if train_count == self.CPU_SAMPLE_SIZE & valid_count == self.CPU_SAMPLE_SIZE & test_count == self.CPU_SAMPLE_SIZE / 2:
                    break

            # init info
            for bag in list(bags):
                if len(bags[bag]) == 0:
                    del bags[bag]
                    del bags_creation_years[bag]
                    del real_birth_years[bag]

            for bag in bags:
                min_cr = torch.min(bags_creation_years[bag]).long()
                max_cr = torch.max(bags_creation_years[bag]).long()
                period = 99 - (max_cr - min_cr)
                birth_ranges[bag] = torch.Tensor([min_cr - period, min_cr])
            min_cr = torch.min(torch.t(torch.cat(list(birth_ranges.values())).view(-1, 2))[0])
            max_cr = torch.max(torch.t(torch.cat(list(birth_ranges.values())).view(-1, 2))[1])
            feasible_years = torch.arange(min_cr.item(), max_cr.item() + 1)
            birth_priors = torch.Tensor([100 / feasible_years.size()[0] / 100]).repeat(feasible_years.size()[0])

            return bags_creation_years, real_birth_years, birth_ranges, feasible_years, birth_priors

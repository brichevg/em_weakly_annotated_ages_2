import torch
from torch import nn, optim
from torch.utils.data import DataLoader

from Net import Net
from loader_for_age_cnn import LoaderForAgeCNN
import sys


def train_agedb(min_mae, model_name, only_test=False):
    loader = LoaderForAgeCNN()
    print("Loading images...")
    loader.load()

    if torch.cuda.is_available():
        device = torch.device('cuda:0')
        print('Using GPU')
    else:
        device = torch.device('cpu')
        print('Using CPU')

    batch_size = 50
    dataloader_test = DataLoader(loader.test, batch_size=batch_size, shuffle=True)
    mae = nn.L1Loss()

    if not only_test:
        model = Net().to(device)
        optimizer = optim.Adam(model.parameters(), lr=0.00001, weight_decay=0.00001)

        # create batches
        dataloader_train = DataLoader(loader.training, batch_size=batch_size, shuffle=True)
        dataloader_valid = DataLoader(loader.validation, batch_size=batch_size, shuffle=True)
        best_validation_mae = 1000

        for epoch in range(1, 20):
            loss_function = nn.NLLLoss()
            train_acc, valid_acc, validation_mae, train_mae = [], [], [], []
            model.train()

            for data, age in dataloader_train:
                data = data.to(device)
                age = (age - 1).to(device)
                data = data.view(-1, 1, 64, 64).float().clone().detach().requires_grad_(True)
                distribution = model(data).view(data.size()[0], 100).to(device)

                # prediction
                batch_prediction = torch.argmax(distribution, dim=1).to(device)

                # loss
                loss = loss_function(distribution.log(), age).to(device)

                train_acc.append(torch.mean((batch_prediction == age).float()).item())
                loss.backward()
                optimizer.step()
                optimizer.zero_grad()
                train_mae.append(mae(batch_prediction.float(), age.float()).item())
            train_mae_mean = (torch.mean(torch.Tensor(train_mae)).float()).item()

            print("Training epoch: ", epoch, " Training MAE: ", train_mae_mean)

            # evaluation part
            model.eval()
            for data, age in dataloader_valid:
                data = data.to(device)
                age = (age - 1).to(device)
                data = data.view(-1, 1, 64, 64).float().clone().detach().requires_grad_(False)
                distribution = model(data).view(data.size()[0], 100).detach().to(device)
                batch_prediction = torch.argmax(distribution, dim=1).to(device)
                valid_acc.append(torch.mean((batch_prediction == age).float()).item())
                validation_mae.append(mae(batch_prediction.float(), age.float()).item())

            validation_mae_mean = (torch.mean(torch.Tensor(validation_mae)).float()).item()
            if validation_mae_mean <= min_mae:
                torch.save(model, model_name+'.pt')
                print(validation_mae_mean)
                break
            if best_validation_mae > validation_mae_mean:
                best_validation_mae = validation_mae_mean
                torch.save(model, model_name+'.pt')

            print("Validation epoch: ", epoch, " MAE: ", validation_mae_mean, "Best MAE: ", best_validation_mae)

    # test
    model = torch.load(model_name+'.pt')
    model.eval()
    test_mae = []
    for data, age in dataloader_test:
        data = data.to(device)
        age = (age - 1).to(device)
        data = data.view(-1, 1, 64, 64).float().clone().detach().requires_grad_(False)
        distribution = model(data).view(data.size()[0], 100).to(device)
        batch_prediction = torch.argmax(distribution, dim=1).to(device)

        test_mae.append(mae(batch_prediction.float(), age.float()).item())
    test_mae_mean = (torch.mean(torch.Tensor(test_mae)).float()).item()

    print(" Test MAE: ", test_mae_mean)


train_agedb(int(sys.argv[1]), sys.argv[2])

import pickle
import torch

from em import EM
from loader import Loader

import sys

if not sys.warnoptions:
    import warnings

    warnings.simplefilter("ignore")


def get_device(cuda_device='cuda:0'):
    if torch.cuda.is_available():
        device = torch.device(cuda_device)
        print('Using GPU')
    else:
        device = torch.device('cpu')
        print('Using CPU')
    return device


def do_em():
    device = get_device()
    try:
        with open('data/loader', 'rb') as pickle_file:
            loader = pickle.load(pickle_file)
    except FileNotFoundError:
        with open("data/loader", "wb") as f:
            print("Creating loader with images..")
            loader = Loader(device)
            pickle.dump(loader, f, pickle.HIGHEST_PROTOCOL)
            print("Loader saved..")
    EM(loader, device).em(epochs=5)


def test_em():
    device = get_device()
    try:
        with open('data/loader', 'rb') as pickle_file:
            loader = pickle.load(pickle_file)
    except FileNotFoundError:
        with open("data/loader", "wb") as f:
            print("Creating loader with images..")
            loader = Loader(device)
            pickle.dump(loader, f, pickle.HIGHEST_PROTOCOL)
            print("Loader saved..")
    model = torch.load('final_model_cpu.pt').to(device) \
        if device.type == 'cpu' else torch.load('final_model_cuda.pt').to(device)
    EM(loader, device).test(model)


#do_em()
test_em()

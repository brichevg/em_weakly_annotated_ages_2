# How to run
1. Unzip the data contained in the `data` directory.
2. Then, do `python3 Main.py`. For testing the model, comment the line `do_EM()` and uncomment `#test_em()`, in `Main.py`.

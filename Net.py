import torch
import torch.nn as nn


def init_weights(m):
    if type(m) == nn.Linear or type(m) == nn.Conv2d:
        torch.nn.init.kaiming_uniform_(m.weight)
        m.bias.data.fill_(0.01)

class Net(nn.Module):

    def __init__(self, n = 100):
        super(Net, self).__init__()

        # 1 input image channel, 21 output channels, 3x3 square convolution kernel

        self.layer1 = nn.Sequential(
            nn.Conv2d(in_channels=1, out_channels=32, kernel_size=3),
            nn.BatchNorm2d(32, track_running_stats=False),
            nn.ReLU(),
            # nn.Dropout2d(p=0.1)
        )
        self.layer1.apply(init_weights)

        self.layer2 = nn.Sequential(
            nn.Conv2d(in_channels=32, out_channels=32, kernel_size=3),
            nn.BatchNorm2d(32, track_running_stats=False),
            nn.ReLU()
        )
        self.layer2.apply(init_weights)

        self.layer3 = nn.MaxPool2d(2, stride=2)

        self.layer4 = nn.Sequential(
            nn.Conv2d(in_channels=32, out_channels=64, kernel_size=3),
            nn.BatchNorm2d(64, track_running_stats=False),
            nn.ReLU(),
            # nn.Dropout2d(p=0.1)
        )
        self.layer4.apply(init_weights)

        self.layer5 = nn.MaxPool2d(2, stride=2)

        self.layer6 = nn.Sequential(
            nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3),
            nn.BatchNorm2d(64, track_running_stats=False),
            nn.ReLU()
        )
        self.layer6.apply(init_weights)

        self.layer7 = nn.MaxPool2d(2, stride=2)

        self.layer8 = nn.Sequential(
            nn.Conv2d(in_channels=64, out_channels=128, kernel_size=3),
            nn.BatchNorm2d(128, track_running_stats=False),
            nn.ReLU()
        )
        self.layer8.apply(init_weights)

        self.layer9 = nn.Sequential(
            nn.Conv2d(in_channels=128, out_channels=256, kernel_size=4),
            nn.BatchNorm2d(256, track_running_stats=False),
            nn.ReLU()
        )
        self.layer9.apply(init_weights)

        self.layer10 = nn.Sequential(
            nn.Conv2d(in_channels=256, out_channels=1024, kernel_size=1),
            nn.BatchNorm2d(1024, track_running_stats=False),
            nn.ReLU()
        )
        self.layer10.apply(init_weights)

        self.layer11 = nn.Sequential(
            nn.Conv2d(in_channels=1024, out_channels=1024, kernel_size=1),
            nn.BatchNorm2d(1024, track_running_stats=False),
            nn.ReLU()
        )
        self.layer11.apply(init_weights)

        self.layer12 = nn.Sequential(
            nn.Conv2d(in_channels=1024, out_channels=n, kernel_size=1),
            nn.BatchNorm2d(n, track_running_stats=False),
            nn.ReLU()
        )

        self.layer12.apply(init_weights)

        self.layer13 = nn.Softmax(dim=1)

    def forward(self, x):
        # In1: (54, 1, 64, 64)
        out = self.layer1(x)
        # In2: (54, 32, 62, 62)
        out = self.layer2(out)
        # In3: (54, 32, 60, 60)
        out = self.layer3(out)
        # In4: (54, 32, 30, 30)
        out = self.layer4(out)
        # In5: (54, 64, 28, 28)
        out = self.layer5(out)
        # In6: (54, 64, 14, 14)
        out = self.layer6(out)
        # In7: (54, 64, 12, 12)
        out = self.layer7(out)
        # In8: (54, 64, 6, 6)
        out = self.layer8(out)
        # In9: (54, 128, 4, 4)
        out = self.layer9(out)
        # In10: (54, 256, 1, 1)
        out = self.layer10(out)
        # In11: (54, 1024, 1, 1)
        out = self.layer11(out)
        # In12: (54, 1024, 1, 1)
        out = self.layer12(out)
        # In13: (54, 100, 1, 1)
        out = self.layer13(out)
        # In14: (54, 100, 1, 1)
        return out

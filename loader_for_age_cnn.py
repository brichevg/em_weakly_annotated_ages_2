import itertools
from csv import reader
import cv2 as cv
import os
import random
import torch as torch

from ImagesDataset import ImagesDataset


class LoaderForAgeCNN:
    def __init__(self):
        self.training = ImagesDataset()
        self.validation = ImagesDataset()
        self.test = ImagesDataset()
        self.all = ImagesDataset()
        self.features_path = 'features/morph_bags_adjusted.csv'

    def load(self):
        with open(self.features_path, 'r') as read_obj:

            data = reader(read_obj)

            for features in itertools.islice(data, 1, None):
                img = cv.imread(os.path.join('data/', features[0]))

                if img is None:
                    continue
                gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

                # bd = real_birth_years[name]
                age = int(features[10])
                folder = int(features[13])

                self.all.push((gray, age))

                if folder in range(4, 11):
                    self.training.push([gray, age])

                elif folder in range(3, 4):
                    self.validation.push([gray, age])

                else:
                    self.test.push([gray, age])

from torch import optim, nn
from torch.utils.data import DataLoader

from expectation import Expectation
import matplotlib

matplotlib.use('Agg')
import matplotlib.pyplot as plt
import torch as torch


class EM:
    def __init__(self, loader, device):
        self.creation_years, self.real_birth_years, self.birth_ranges, self.feasible_years, self.birth_priors = loader.get()
        self.train_data = DataLoader(loader.training, batch_size=32, shuffle=True)
        self.valid_data = DataLoader(loader.validation, batch_size=32, shuffle=True)
        self.test_data = DataLoader(loader.test, batch_size=32, shuffle=True)

        self.all = DataLoader(loader.all, batch_size=32, shuffle=True)

        self.device = device
        self.e_layer = Expectation(self.device,
                                   self.feasible_years,
                                   self.birth_ranges,
                                   self.creation_years).to(self.device)

    def e(self, model, valid=False, test=False):

        # return indx in original dataset, age distr, names, cr times
        idx, d, n, cr = self.eval_images(model, valid, test)

        # all by bags
        idx_b, d_b, n_b, cr_b = self.to_bags(d, n, idx=idx, cr=cr)

        sizes = [d_b[key].size()[0] for key in d_b.keys()]

        a = self.e_layer.forward(d_b, self.birth_priors)

        # b by bags
        b = self.find_b(d_b, a, cr_b)

        b_all = torch.zeros(d.size()).to(self.device)

        for name in n_b.keys():
            b_all[idx_b[name]] = b[name]

        return b_all.to(self.device), a, sizes

    def to_bags(self, d, n, idx=None, b=None, cr=None):
        d_b = {}
        idx_b = {}
        n_b = {}
        b_b = {}
        cr_b = {}
        # group by bags for the beta calculation
        for i in range(0, len(n)):
            bag = n[i]
            if bag not in n_b:
                d_b[bag] = torch.Tensor().to(self.device)
                b_b[bag] = torch.Tensor().to(self.device)
                idx_b[bag] = torch.Tensor().long().to(self.device)
                n_b[bag] = []
                cr_b[bag] = torch.Tensor().long().to(self.device)
            d_b[bag] = torch.cat((d_b[bag], d[i]))

            if cr is not None:
                cr_b[bag] = torch.cat((cr_b[bag], torch.Tensor([cr[i]]).long().to(self.device)))

            if b is not None:
                b_b[bag] = torch.cat((b_b[bag], b[i]))

            if idx is not None:
                idx_b[bag] = torch.cat((idx_b[bag], torch.Tensor([idx[i]]).long().to(self.device)))

            if n is not None:
                n_b[bag].append(bag)

        for i in d_b:
            d_b[i] = d_b[i].view(-1, 100)

        for i in b_b:
            b_b[i] = b_b[i].view(-1, 100)

        if b is None:
            return idx_b, d_b, n_b, cr_b
        else:
            return d_b, b_b

    def find_b(self, d, a, cr_all):
        b = {}
        oldest = self.feasible_years[0]
        for bag_i in d:
            cr = cr_all[bag_i]
            ages = torch.arange(0, 100).repeat(d[bag_i].size()[0], 1).long().to(self.device)
            diff_idx = (torch.t(cr.repeat(ages.size()[1], 1)) - ages)
            alpha_idx_feasible = ((diff_idx >= self.birth_ranges[bag_i][0]) & (diff_idx <= self.birth_ranges[bag_i][1]))
            b[bag_i] = torch.Tensor([oldest]).long().repeat(diff_idx.size()[0], diff_idx.size()[1]).to(self.device)
            b[bag_i][alpha_idx_feasible] = diff_idx[alpha_idx_feasible]
            b[bag_i] = (b[bag_i] - oldest)
            b[bag_i][alpha_idx_feasible] = a[bag_i][b[bag_i][alpha_idx_feasible].long()]
        return b

    def loss(self, d, b):
        size = d.size()[0]
        ages = torch.arange(0, 100).repeat(size, 1).long().clone().to(self.device)
        img_idx = torch.t(torch.arange(0, size).repeat(ages.size()[1], 1)).long().clone()
        bag_age_d = d[img_idx, ages].to(self.device).clone()
        return torch.sum(torch.sum((b * torch.log(bag_age_d)), 1)).to(self.device)

    def m(self, model, opt, b, valid=False, test=False):
        if valid:
            epochs = 1
            dataset = self.valid_data
            model.eval()
        elif test:
            epochs = 1
            dataset = self.test_data
            model.eval()
        else:
            epochs = 5
            dataset = self.train_data
            model.train()

        for epoch in range(0, epochs):
            loss_num = 0
            for data, n, _, idx in dataset:
                data = data.to(self.device)
                d = model(data.view(-1, 1, 64, 64).float()).view(data.size()[0], 100).to(self.device)
                d_bags, b_bags = self.to_bags(d, n=n, b=b[idx])
                loss = 0
                for bag in d_bags:
                    bag_loss = self.loss(d_bags[bag], b_bags[bag])
                    loss = loss - bag_loss
                loss_num = loss_num + loss.item()
                if not test and not valid:
                    loss.backward(retain_graph=True)
                    opt.step()
                    opt.zero_grad()
            if not valid and not test:
                print("Training epoch ", epoch, ":", loss_num)
        if valid:
            print("Validation loss: ", loss_num)
            return model, loss_num
        elif test:
            print("Test loss: ", loss_num)
            return loss_num
        else:
            return model, loss_num

    def update_priors(self, a, sizes):
        sizes = torch.t(torch.Tensor([sizes]).squeeze().repeat(self.birth_priors.size()[0], 1)).to(self.device)
        if torch.sum(sizes * torch.stack([a[key].detach() for key in a.keys()])) == 0:
            self.birth_priors = torch.sum(sizes * torch.stack([a[key].detach() for key in a.keys()]), dim=0) \
                                / torch.sum(sizes * torch.stack([a[key].detach() for key in a.keys()]))

    def eval_images(self, model, valid=False, test=False):
        model.eval()

        d = torch.Tensor().to(self.device)
        idx = torch.Tensor().long().to(self.device)
        names = []
        cr = torch.Tensor().long().to(self.device)

        # images, name, creation, index in the initial dataset
        lists = []

        if valid:
            dataset = self.valid_data
        elif test:
            dataset = self.test_data
        else:
            dataset = self.train_data

        for data, name_b, cr_b, idx_b in dataset:
            lists.append(idx_b)
            data = data.to(self.device)
            d = torch.cat(
                (d, model(data.view(-1, 1, 64, 64).float()).view(data.size()[0], 100).detach().to(self.device)))
            cr = torch.cat((cr, cr_b.to(self.device)))
            idx = torch.cat((idx, idx_b.to(self.device)))
            names = names + list(name_b)
        return idx, d, names, cr

    def em(self, epochs):
        # model = Net().to(self.device)
        model = torch.load('initial_model.pt').to(self.device)
        opt = optim.Adam(model.parameters(), lr=0.005, weight_decay=0.0001)
        all_v_loss = []
        all_t_loss = []

        # validation just to see initial result
        b, a, _ = self.e(model, valid=True)
        _, loss = self.m(model, opt, b.detach(), valid=True)
        print("Validation MAE start: ", self.mae(a))

        for epoch in range(0, epochs):
            print("EM epoch: ", epoch)
            # train
            b, a, sizes = self.e(model)
            model, t_loss = self.m(model, opt, b.detach())
            all_t_loss.append(t_loss)
            self.update_priors(a, sizes)

            # validation
            b, a, _ = self.e(model, valid=True)
            model_, loss_ = self.m(model, opt, b.detach(), valid=True)
            all_v_loss.append(loss_)
            print("Validation MAE: ", self.mae(a))
            if loss_ < loss:
                loss = loss_
                torch.save(model_, 'final_model_cuda.pt')
                model_.to('cpu')
                torch.save(model_, 'final_model_cpu.pt')
                model_.to(self.device)
        # test
        model = torch.load('final_model_cuda.pt')
        self.test(model)

        # plot loss
        self.plot_loss(all_v_loss, all_t_loss, epochs)

    def test(self, model):
        # test
        b, a, _ = self.e(model, test=True)

        mae = self.mae(a)
        loss = self.m(model, None, b.detach(), test=True)
        print("Test loss: ", loss, ", MAE: ", mae)
        self.plot(a)

    def mae(self, a):
        keys = list(a.keys())
        mae = 0
        for key in keys:
            mae = mae + abs(self.feasible_years[torch.argmax(a[key])].long() - self.real_birth_years[key])
        return (mae / len(keys)).item()

    def plot(self, a):
        f = plt.figure(0)
        keys = list(a.keys())
        bag_i = 0
        for bag_i_x in range(0, 4):
            for bag_i_y in range(0, 4):
                if bag_i == len(keys):
                    break
                plt.subplot2grid((4, 4), (bag_i_x, bag_i_y))
                plt.plot(self.feasible_years.cpu().clone().numpy(),
                         a[keys[bag_i]].detach().cpu().clone().numpy())
                diff = abs(self.feasible_years[torch.argmax(a[keys[bag_i]])].long() \
                           - self.real_birth_years[keys[bag_i]])
                plt.axvline(x=self.real_birth_years[keys[bag_i]], color='red')
                plt.title("mae: " + str(diff.item()))
                plt.yticks([])
                bag_i = bag_i + 1
        # plt.show()
        plt.tight_layout()
        plt.draw()
        f.savefig('plots_final.png')
        plt.clf()

    def plot_loss(self, v_loss, t_loss, epochs):
        f = plt.figure(0)
        plt.plot(list(range(0, epochs)), t_loss, 'r', label='train')
        plt.plot(list(range(0, epochs)), v_loss, 'g', label='validation')
        plt.legend()
        plt.xticks(range(0, epochs))
        plt.xlabel('epochs')
        plt.ylabel('loss')
        plt.tight_layout()
        plt.draw()
        f.savefig('loss.png')
        plt.clf()

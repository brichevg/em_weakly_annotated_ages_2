import torch as torch


class Expectation(torch.nn.Module):

    def __init__(self, device, feasible_birth_years, bag_birth_ranges, bag_creation_years):
        super(Expectation, self).__init__()
        self.device = device
        self.feasible_birth_years = feasible_birth_years
        self.bag_birth_ranges = bag_birth_ranges
        self.bag_creation_years = bag_creation_years
        self.smallest =  100000

    def forward(self, d, birth_priors):
        a = {}
        for bag_i in d:
            birth_range = self.bag_birth_ranges[bag_i]
            # first component
            first = torch.log(birth_priors).to(self.device)


            # second component
            br = torch.t(self.feasible_birth_years.repeat(self.bag_creation_years[bag_i].size()[0], 1))
            cr = self.bag_creation_years[bag_i].repeat(self.feasible_birth_years.size()[0], 1)
            ages = cr - br
            feasible_ages = (ages >= 0) & (ages < 100)

            # which birth dates are eligible for this bag of photos (creation times - birth date should be [0,
            # 100] for each photo, cause if model(creat_x - birth|x) is 0, then  log(0) is -inf and the whole summation
            # will be - inf)
            fit_range = torch.prod(feasible_ages, dim=1).nonzero().view(-1)

            # repeating for the vectorization in formula
            # 1.repeating years for each photo
            years = self.feasible_birth_years[torch.prod(feasible_ages, dim=1).nonzero()].repeat(1, self.bag_creation_years[bag_i].size()[0])

            # 2.repeating creation times for each image accross years
            creations = self.bag_creation_years[bag_i].repeat(years.size()[0], 1)

            # age indices for each image
            age_i = torch.t(creations - years)
            img_i = torch.t(torch.arange(0, d[bag_i].size()[0]).repeat(fit_range.size()[0], 1))
            second = torch.Tensor([float('-inf')]).repeat(self.feasible_birth_years.size()).to(self.device)
            second[fit_range] = torch.sum(torch.log(d[bag_i][img_i.long(), age_i.long()]), dim=0)


            # third
            bag_f_b = torch.arange(birth_range[0].item(), birth_range[1].item() + 1)

            age_i = torch.t(self.bag_creation_years[bag_i].repeat(bag_f_b.size()[0], 1)) - bag_f_b.repeat(self.bag_creation_years[bag_i].size()[0], 1)
            img_i = torch.t(torch.arange(0, d[bag_i].size()[0]).repeat(bag_f_b.size()[0], 1))
            age_p_prod = torch.prod(d[bag_i][img_i.long(), age_i.long()].double(), dim=0) + 1e-310
            bag_birth_p = birth_priors[(self.feasible_birth_years >= birth_range[0]) & (self.feasible_birth_years <= birth_range[1])].to(self.device)
            # print(torch.log(torch.sum(age_p_prod * bag_birth_p)))
            # print(torch.log(torch.sum(age_p_prod * bag_birth_p )))
            sum = torch.sum(age_p_prod * bag_birth_p).to(self.device)
            third = torch.log(sum).to(self.device)
            a[bag_i] = torch.exp(first + second - third).to(self.device)        
        return a
